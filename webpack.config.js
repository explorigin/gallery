var webpack = require('webpack');
var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');

var isProduction = process.env.NODE_ENV == 'production';

var devEntries = isProduction ? [] : [
    // 'webpack-dev-server/client?http://0.0.0.0:8080'
    // 'webpack/hot/only-dev-server'
];

var uglifyJSOptions = {
    comments: false,
    mangle: true,
};

var commonPlugins = [
    new ExtractTextPlugin('app.css', { allChunks: true }),
    new HtmlWebpackPlugin({
        title: 'Gallery',
        template: 'index.html',
        minify: {
            removeComments: isProduction,
            collapseWhitespace: isProduction,
        },
    }),
    new webpack.optimize.CommonsChunkPlugin({
        name: 'commons',
        filename: 'commons.js'
    }),
];
var plugins = isProduction ? [
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin(uglifyJSOptions),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.AggressiveMergingPlugin(),
    new webpack.optimize.MinChunkSizePlugin({minChunkSize: 10000})
] : [
    new webpack.NoErrorsPlugin()
];

module.exports = {
    context: path.join(__dirname, 'src'),
    entry: devEntries.concat(['./index.js']),
    devtool: process.env.WEBPACK_DEVTOOL || 'source-map',
    output: {
        path: path.join(__dirname, 'public'),
        filename: 'bundle.js'
    },
    module: {
        preLoaders: [
            {
                test: /\.js?$/,
                exclude: /node_modules/,
                loader: 'eslint-loader'
            }
        ],
        loaders: [
            {
                test: /\.js?$/,
                exclude: /node_modules/,
                loaders: ['babel'],
            },
            { test: /\.png$/, loader: 'url-loader?limit=100000' },
            { test: /\.jpg$/, loader: 'file-loader' },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract(
                    'style',
                    'css?-modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]'
                )
            },
            //font-awesome
            { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'url-loader?limit=10000&minetype=application/font-woff' },
            { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'file-loader' }
        ]
    },
    devServer: {
        contentBase: './public',
        noInfo: true,
        hot: false,
        inline: true,
        port: 9090
    },
    plugins: commonPlugins.concat(plugins)
};
