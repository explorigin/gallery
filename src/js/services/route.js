import { Router } from 'starlight-js';

export function getRouter(callback) {
    return Router(
        '#',
        {
            home: {
                path: '/',
                onenter: callback
            },
            image: {
                path: '/image/:image',
                vars: { image: /.*/ },
                onenter: callback
            },
            // album: {
            //     path: '/album/:album/:image',
            //     vars: { image: /.*/, album: /[A-z]+/ },
            //     onenter: function (r, route) {
            //         const { image, album } = route.vars;
            //         app.update({ image, album });
            //     }
            // }
        }
    );
}
