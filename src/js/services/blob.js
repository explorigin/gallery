const blobUrlMap = new Map();


export function setBlobUrl(id, data) {
    if (!blobUrlMap.has(id)) {
        blobUrlMap.set(id, URL.createObjectURL(data));
    }
    return getBlobUrl(id);
}

export function getBlobUrl(id) {
    return blobUrlMap.get(id);
}

export function revokeBlobUrl(id) {
    URL.revokeObjectURL(blobUrlMap.get(id));
    blobUrlMap.delete(id);
};
