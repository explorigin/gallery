import { Icon } from './Icon';

export function IconButton(api, props) {
    const { h } = api;
    const { name, size, onclick, buttonProps, iconProps } = props;

    return function render() {
        return h('.button',
            { onclick, ...(buttonProps || {}) },
            h(Icon, { name, size, ...(iconProps || {}) })
        );
    };
}
