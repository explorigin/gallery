import { injectStyle } from '../utils/styletron';


const CSS_DROPZONE = {
    width: '200px',
    height: '200px',
    border: '2px #666 dashed',
    borderRadius: '5px',
};
const CSS_DROPZONE_ACTIVE = {
    borderStyle: 'solid',
    backgroundColor: '#eee',
};

export function Dropzone(api, props) {
    const { redraw, prop, h } = api;
    const {
        className,
        activeClassName,
        ondrop,
        ondragenter,
        ondragleave,
        class: _class,
        ...wrapperProps,
    } = props;

    const enterCounter = prop(0, redraw);

    function onDragOver() {
        // This needs to be here so Starlight will run evt.preventDefault()
        // which allows the browser to accept drops.
    }

    function onDragEnter() {
        enterCounter(enterCounter() + 1);

        if (ondragenter) {
            ondragenter();
        }
    }

    function onDragLeave() {
        enterCounter(enterCounter() - 1);

        if (ondragleave) {
            ondragleave();
        }
    }

    function onDrop(evt) {
        enterCounter(0);

        if (ondrop) {
            ondrop(evt.dataTransfer.files);
        }
    }

    return function render(children) {
        return [
            h('div',
                {
                    class: {
                        [className || injectStyle(CSS_DROPZONE)]: true,
                        [activeClassName || injectStyle(CSS_DROPZONE_ACTIVE)]: enterCounter() > 0,
                        ...(_class || {}),
                    },
                    ondragenter: onDragEnter,
                    ondragover: onDragOver,
                    _ondragover_flags: ['preventDefault'],
                    ondragleave: onDragLeave,
                    ondrop: onDrop,
                    _ondrop_flags: ['preventDefault', 'dataTransfer.files'],
                    ...wrapperProps,
                },
                ...children
            )
        ];
    };
}
