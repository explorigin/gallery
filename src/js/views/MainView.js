import * as store from '../store';
import { AlbumView } from './AlbumView';


export function MainView(api, props) {
    const { updateChildContext, handleUpdate, redraw, h } = api;

    let album = props.album || 'index';
    let focused = null;
    let total = 0;
    let images = [];
    let loading = true;
    let imageData = new Map();
    let imageIds = [];

    function updateFocused(imageId) {
        const focusedId = `image_${imageId}`;

        if (imageId) {
            if (imageData.has(focusedId)) {
                focused = focusedId;
            } else {
                api.goto('home');
            }
        } else {
            focused = null;
        }
    }

    handleUpdate((nextProps) => {
        updateFocused(nextProps.image);
    });

    function refresh() {
        store.getAlbum(album).then(result => {
            loading = false;
            total = result.total;
            images = result.images;
            imageData = new Map(images.map(i => ([i._id, i])));
            imageIds = Array.from(imageData.keys());
            updateFocused(props.image);
            redraw();
        });
    }

    store.subscribe(refresh);
    refresh();

    return function render() {
        return [
            h(AlbumView, {
                loading,
                total,
                imageData,
                imageIds,
                focused,
            }),
        ];
    };
}
