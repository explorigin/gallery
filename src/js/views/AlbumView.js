import moment from 'moment';
import partition from 'linear-partitioning';

import { Dropzone } from '../components/Dropzone';
import { Icon } from '../components/Icon';
import { IconButton } from '../components/IconButton';
import { ImageViewerView } from './ImageViewerView';
import * as store from '../store';
import * as statics from '../statics';
import * as blobService from '../services/blob';
import { styled, injectStyle } from '../utils/styletron';
import { observable, computed } from '../utils/functional';


const OPTIMAL_IMAGE_HEIGHT = 140;
const CONTENT_MARGIN = 12;
const IMAGE_MARGIN = 2;
const ROW_HEIGHT_CUTOFF_MODIFIER = 2;

const IMAGE_MARGIN_WIDTH = 2 * IMAGE_MARGIN;

const toSectionId = (id) => (
    new Date(
        parseInt(
            id.split('_')[1],
            36
        )
    ).toISOString().substr(0, 10)
);
const routeImageId = id => id.substr(6);
const aspectRatio = img => (img.width - IMAGE_MARGIN_WIDTH) / (img.height - IMAGE_MARGIN_WIDTH);

export function AlbumView(api, props) {
    const {
        redraw,
        memoize,
        handleUpdate,
        h
    } = api;
    let {
        imageData,
    } = props;

    const scrolledToTop = observable(true, redraw);
    const selected = new Set();
    const selectedSections = new Set();
    const viewerLayout = observable({width: 0, height: 0}, redraw);
    const imageHover = observable(null, redraw);
    const sectionHover = observable(null, redraw);
    let firstRender = true;

    const total = observable(props.total);
    const focused = observable(props.focused);
    const imageIds = observable(props.imageIds);
    const focusedIndex = computed(
        [focused, imageIds],
        (focusedId, imageIdArray) => (
            focusedId ? imageIdArray.indexOf(focusedId) : -1
        )
    );

    const sections = computed(
        [imageIds],
        imageIdArray => (
            imageIdArray.reduce((obj, id) => {
                const date = toSectionId(id);
                return {
                    ...obj,
                    [date]: (obj[date] || []).concat(id)
                };
            }, {})
        )
    );

    const orderedSectionIds = computed(
        [sections],
        (sectionMap) => (
            Object.keys(sectionMap).sort((a, b) => (a.localeCompare(b)))
        )
    );

    const sectionRows = computed(
        [orderedSectionIds, sections],
        (sIds, sectionMap) => (
            sIds.reduce((obj, sId) => {
                return {
                    ...obj,
                    [sId]: calculatePartitions(sectionMap[sId])
                };
            }, {})
        )
    );

    function calculatePartitions(imgIds) {
        const images = imgIds.map(id => imageData.get(id));
        const availableWidth = viewerLayout().width - CONTENT_MARGIN * 2;
        const totalImageRatio = images
            .reduce((acc, img) => acc + aspectRatio(img), 0);
        const rowCount = Math.ceil(totalImageRatio * OPTIMAL_IMAGE_HEIGHT / availableWidth);
        const rowRatios = partition(
            images.map(i => (i.width / i.height)),
            rowCount
        );

        let index = 0;

        const result = rowRatios.map((row, rowIndex, rows) => {
            const rowTotal = row.reduce((acc, r) => (acc + r), 0);
            const imageRatio = row[0];
            const portion = imageRatio / rowTotal;
            let rowHeight = availableWidth * portion / aspectRatio(imageData.get(imgIds[index]));
            if (rowHeight > OPTIMAL_IMAGE_HEIGHT * ROW_HEIGHT_CUTOFF_MODIFIER) {
                rowHeight = OPTIMAL_IMAGE_HEIGHT * ROW_HEIGHT_CUTOFF_MODIFIER;
            }

            const rowResult = row.map((imageRatio, imgIndex) => {
                const id = imgIds[imgIndex + index];
                const img = imageData.get(id);
                let width = aspectRatio(img) * rowHeight;
                let height = rowHeight;

                return [id, img, width, height];
            });

            index += rowResult.length;
            return rowResult;
        });
        return result;
    }

    handleUpdate((nextProps, prevProps) => {
        const imageIdSet = new Set(nextProps.imageIds);
        const removedImageIds = prevProps.imageIds
            .filter(pId => !imageIdSet.has(pId) && selected.has(pId));

        function setData(shouldRedraw) {
            imageData = nextProps.imageData;
            imageIds(nextProps.imageIds);
            focused(nextProps.focused);
            total(nextProps.total);
            removedImageIds.forEach(deletedId => {
                toggleImage(deletedId)();
                blobService.revokeBlobUrl(prevProps.imageData.get(deletedId).imageUrl);
            });
            if (shouldRedraw) {
                redraw();
            }
        }

        function setBlobs() {
            store.getAttachments(nextProps.imageIds, 'image')
            .then(attachments => {
                attachments.forEach(({ id, attachment }) => (
                    blobService.setBlobUrl(nextProps.imageData.get(id).imageUrl, attachment)
                ));
                setData(true);
            });
        }
        if (nextProps.total !== total()) {
            setBlobs();
        } else {
            setData(false);
        }
    });

    function ingestDroppedFiles(files) {
        store.saveImagesToImport(files);
    }

    const deleteSingleImage = memoize((id) => () => {
        store.deleteImages([id]);
    });

    function deleteImages() {
        store.deleteImages(selected);
    }

    const toggleSection = memoize((sId) => () => {
        if (selectedSections.has(sId)) {
            selectedSections.delete(sId);
            sections()[sId].filter(id => selected.has(id))
            .map(id => selected.delete(id));
        } else {
            selectedSections.add(sId);
            sections()[sId].filter(id => !selected.has(id))
            .map(id => selected.add(id));
        }
        redraw();
    });

    const toggleImage = memoize((id) => () => {
        const sId = toSectionId(id);
        if (selected.has(id)) {
            selected.delete(id);
            if (selectedSections.has(sId)
                && sections()[sId]
                && sections()[sId].some(iId => !selected.has(iId))
            ) {
                selectedSections.delete(sId);
            }
        } else {
            selected.add(id);
            if (!selectedSections.has(sId)
                && sections()[sId]
                && sections()[sId].every(iId => selected.has(iId))
            ) {
                selectedSections.add(sId);
            }
        }
        redraw();
    });

    const handleImageClick = memoize((id) => () => {
        if (selected.size) {
            return toggleImage(id)();
        } else if (focused() !== id) {
            api.goto('image', {image: routeImageId(id)});
        } else {
            api.goto('home');
        }
    });

    const onClickNext = memoize((id) => {
        if (focusedIndex() === -1 || focusedIndex() === imageIds().length - 1) {
            return null;
        }
        return () => api.goto('image', {image: routeImageId(imageIds()[focusedIndex() + 1])});
    });

    const onClickPrev = memoize((id) => {
        if (focusedIndex() < 1) {
            return null;
        }
        return () => api.goto('image', {image: routeImageId(imageIds()[focusedIndex() - 1])});
    });

    function handleScroll(evt) {
        scrolledToTop(evt.target.scrollTop === 0);
    }

    const handleImageHover = memoize(id => {
        return () => imageHover(id);
    });

    const handleSectionHover = memoize(id => {
        return () => sectionHover(id);
    });


    function renderImage([id, img, width, height]) {
        const selectMode = !!selected.size;
        const isSelected = selected.has(id);
        const _imageHover = imageHover() === id;
        const src = (
            blobService.getBlobUrl(img.thumbnailURL || img.imageUrl)
            || statics.UNLOADED_THUMBNAIL
        );

        return imageContainer(
            {
                onmouseover: handleImageHover(id),
                onmouseout: handleImageHover(null),
                onclick: handleImageClick(id),
                css: {
                    cursor: selectMode ? 'pointer' : 'zoom-in'
                },
                style: `
                    height: ${height}px;
                    width: ${width}px;
                `
            },
            image({ src, css: isSelected ? { transform: 'translateZ(-50px)' } : {} }),
            h(IconButton, {
                onclick: toggleImage(id),
                name: (selectMode && !isSelected) ? 'circle_o' : 'check_circle',
                size: 0.75,
                buttonProps: {
                    css: {
                        position: 'absolute',
                        top: '4%',
                        left: '4%',
                        zIndex: 2,
                        display: 'flex',
                        transition: 'transform .135s cubic-bezier(0.0,0.0,0.2,1), opacity .135s cubic-bezier(0.0,0.0,0.2,1)',
                        borderRadius: '50%',
                        padding: '2px',
                        backgroundColor: isSelected ? 'white' : 'transparent',
                        opacity: isSelected ? 1 : selectMode || _imageHover ? 0.7 : 0,
                        cursor: 'pointer'
                    }
                },
                iconProps: {
                    css: {
                        fill: isSelected ? '#00C800' : '#fff',
                    }
                }
            }),
            overlay({
                css: {
                    transform: isSelected ? 'translateZ(-50px)' : null,
                    opacity: selectMode || _imageHover ? 0.7 : 0,
                }
            }),
        );
    }

    function renderSectionRow(images) {
        return imagesContainer(
            images.map(renderImage)
        );
    }

    function renderSection(sId) {
        const selectMode = !!selected.size;
        const isSelected = selectedSections.has(sId);

        return section(
            {
                onmouseenter: handleSectionHover(sId),
                onmouseleave: handleSectionHover(null),
            },
            sectionTitle(
                moment(sId).format('LL'),
                h(IconButton, {
                    onclick: toggleSection(sId),
                    name: 'check_circle',
                    size: 0.25,
                    buttonProps: {
                        css: {
                            transition: 'transform .135s cubic-bezier(0.0,0.0,0.2,1), opacity .135s cubic-bezier(0.0,0.0,0.2,1)',
                            opacity: isSelected ? 1 : selectMode || sectionHover() === sId ? 0.7 : 0,
                            cursor: 'pointer',
                            paddingLeft: '0.5em'
                        }
                    },
                    iconProps: {
                        css: {
                            fill: isSelected ? '#00C800' : '#000',
                        }
                    }
                })
            ),
            sectionRows()[sId].map(renderSectionRow),
        );
    }

    return function render() {
        const selectMode = !!selected.size;
        const { width, height } = viewerLayout();
        const _focused = focused();

        return [
            h(Dropzone,
                {
                    className: slate,
                    activeClassName: 'dropHover',
                    ondrop: ingestDroppedFiles,
                    _onLayout: viewerLayout,
                },
                total()
                ? [
                    header(
                        {
                            css: {
                                boxShadow: scrolledToTop() ? 'none' : '0px 0px 7px gray'
                            }
                        },
                        h('div', { css: { fontSize: '20pt' } }, 'Gallery'),
                        headerRight(
                            {
                                css: {
                                    visibility: selectMode ? 'visible' : 'hidden'
                                }
                            },
                            `Selected ${selected.size} item${selected.size !== 1 ? 's' : ''}`,
                            h(IconButton, {
                                onclick: deleteImages,
                                name: 'trash',
                                size: 0.75,
                                buttonProps: {
                                    css: {
                                        'marginLeft': '1em',
                                        'opacity': 0.7,
                                        'cursor': 'pointer',
                                        ':hover': {
                                            opacity: 1,
                                        }
                                    }
                                }
                            })
                        ),
                    ),
                    content(
                        {
                            onscroll: handleScroll,
                            _onscroll_flags:['target.scrollTop'],
                        },
                        (
                            width > 0
                            ? orderedSectionIds().map(renderSection)
                            : null
                        ),
                    ),
                ]
                : downloadPane(
                    h(Icon, {
                        name: 'upload',
                        size: 4,
                    }),
                    h('h1', 'Drop pictures here to upload'),
                    h('h2', 'You have no images here.'),
                )
            ),
            _focused
            ? h(ImageViewerView, {
                onClose: handleImageClick(_focused),
                onDelete: deleteSingleImage(_focused),
                onNext: onClickNext(_focused),
                onPrev: onClickPrev(_focused),
                img: imageData.get(_focused),
                viewerHeight: height,
                viewerWidth: width,
            })
            : null
        ];
    };
}

const CSS_FULL_SIZE = {
    width: '100%',
    height: '100%',
};

const slate = injectStyle({
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    overflow: 'hidden',
});

const downloadPane = styled({
    alignItems: 'center',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    display: 'flex',
});

const content = styled({
    display: 'flex',
    overflow: 'auto',
    flex: 1,
    flexDirection: 'column',
});

const image = styled({
    ...CSS_FULL_SIZE,
    position: 'absolute',
    top: 0,
    left: 0,
    transition: 'transform .135s cubic-bezier(0.0,0.0,0.2,1)'
}, 'img');

const overlay = styled({
    ...CSS_FULL_SIZE,
    position: 'absolute', // Unnecessary but helps with a rendering bug in Chrome. https://gitlab.com/explorigin/gallery/issues/1
    top: '0px',
    left: '0px',
    zIndex: 1,
    transition: 'transform .135s cubic-bezier(0.0,0.0,0.2,1), opacity .135s cubic-bezier(0.0,0.0,0.2,1)',
    backgroundImage: 'linear-gradient(to bottom,rgba(0,0,0,0.26),transparent 56px,transparent)',
});

const header = styled({
    justifyContent: 'space-between',
    padding: '1em',
    zIndex: 1,
    display: 'flex',
    alignItems: 'center',
});

const headerRight = styled({
    display: 'flex',
    alignItems: 'center'
});

const section = styled({
    margin: `${CONTENT_MARGIN}px`,
});

const sectionTitle = styled({
    display: 'flex',
});

const imagesContainer = styled({
    display: 'flex',
    alignItems: 'flex-start',
    userSelect: 'none',
});

const imageContainer = styled({
    position: 'relative',
    perspective: '1000px',
    backgroundColor: '#eee',
    margin: `${IMAGE_MARGIN}px`,
});
