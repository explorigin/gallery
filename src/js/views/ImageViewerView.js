import { IconButton } from '../components/IconButton';
import { Icon } from '../components/Icon';
import * as blobService from '../services/blob';
import { styled } from '../utils/styletron';
import { observable } from '../utils/functional';


const ZOOM_CLASSES = [false, 'cover', 'full'];


export function ImageViewerView(api, props) {
    const { redraw, h } = api;
    const { img, onClose, onDelete, onNext, onPrev, viewerHeight, viewerWidth } = props;
    const { width, height } = img;

    const imageUrl = blobService.getBlobUrl(img.imageUrl);
    const mouseActive = observable(true, redraw);
    const zoomLevel = observable(0, (zl) => {
        redraw();
        const imageHeight = viewerWidth * height / width;
        if (zl === 1) {
            scrollTop = imageHeight / 2 - viewerHeight / 2;
            scrollLeft = null;
        } else if (zl === 2) {
            scrollTop = height / 2 - viewerHeight / 2;
            scrollLeft = width / 2 - viewerWidth / 2;
        } else {
            scrollTop = scrollLeft = null;
        }
        redraw();
    });

    let scrollTop = null;
    let scrollLeft = null;
    let mouseMoveTimeout = null;

    const onMouseLeave = () => {
        mouseActive(false);
    };

    const onMouseMove = () => {
        if (mouseMoveTimeout !== null) {
            clearTimeout(mouseMoveTimeout);
        }
        mouseMoveTimeout = setTimeout(onMouseLeave, 3000);
        mouseActive(true);
    };

    const zoomIn = () => {
        zoomLevel(Math.min(zoomLevel() + 1, ZOOM_CLASSES.length - 1));
    };

    const zoomOut = () => {
        zoomLevel(Math.max(zoomLevel() - 1, 0));
    };

    return function render() {
        const zoomImage = zoomImages[zoomLevel()];
        const zoomStyle = [
            `background-image: url(${imageUrl})`,
            `background-image: url(${imageUrl}); height: ${viewerWidth * height / width}px`,
            `background-image: url(${imageUrl}); height: ${height}px; width: ${width}px`,
        ][zoomLevel()];

        return viewer(
            {
                onclick: zoomIn,
                oncontextmenu: zoomOut,
                _oncontextmenu_flags: ['preventDefault'],
                onmousemove: onMouseMove,
                onmouseleave: onMouseLeave,
                scrollTop,
                scrollLeft,
            },
            header(
                { css: mouseActive() ? { opacity: 1 } : {} },
                h(IconButton, {
                    onclick: onClose,
                    name: 'arrow_left',
                    size: 0.75,
                    iconProps: {
                        css: CSS_ICON
                    }
                }),
                h(IconButton, {
                    onclick: onDelete,
                    name: 'trash',
                    size: 0.75,
                    iconProps: {
                        css: CSS_ICON
                    }
                }),
            ),
            zoomImage({ style: zoomStyle }),
            (
            onPrev
            ? prevClickZone(
                { onclick: onPrev },
                h(Icon, {
                    name: 'chevron_left',
                    size: 0.75,
                    css: CSS_ICON,
                })
            )
            : null
            ),
            (
            onNext
            ? nextClickZone(
                { onclick: onNext },
                h(Icon, {
                    name: 'chevron_right',
                    size: 0.75,
                    css: CSS_ICON,
                })
            )
            : null
            ),
        );
    };
}


// Style sets
const CSS_FIXED_FULL_WIDTH = {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    zIndex: 1,
};
const CSS_ZOOM_IMAGE = {
    background: 'top no-repeat #444',
    backgroundSize: 'contain',
    width: '100%',
    height: '100%',
    margin: '0px',
};
const CSS_CLICK_ZONE = {
    'position': 'absolute',
    'width': '33%',
    'height': '70%',
    'display': 'flex',
    'alignItems': 'center',
    'padding': '2em',
    'top': '15%',
    'transition': 'opacity .13s cubic-bezier(0.0,0.0,0.2,1)',
    'opacity': 0,
    'cursor': 'pointer',
    ':hover': {
        opacity: 1
    }
};
const CSS_ICON = {
    cursor: 'pointer',
    opacity: 1,
    fill: 'white',
};


// Styled Tags
const viewer = styled({
    ...CSS_FIXED_FULL_WIDTH,
    bottom: 0,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    userSelect: 'none',
    overflow: 'auto',
});

const header = styled({
    ...CSS_FIXED_FULL_WIDTH,
    height: '64px',
    padding: '1em 2em',
    backgroundImage: 'linear-gradient(0deg,rgba(0,0,0,0),rgba(0,0,0,0.4))',
    transition: 'opacity .5s cubic-bezier(0.0,0.0,0.2,1)',
    opacity: 0,
    justifyContent: 'space-between',
    padding: '1em',
    zIndex: 1,
    display: 'flex',
    alignItems: 'center',
});

const zoomImages = [
    styled(CSS_ZOOM_IMAGE),
    styled({
        ...CSS_ZOOM_IMAGE,
        alignSelf: 'flex-start',
    }),
    styled({
        ...CSS_ZOOM_IMAGE,
        position: 'absolute',
        top: '0px',
        left: '0px',
        backgroundSize: 'cover',
    }),
];

const prevClickZone = styled({
    ...CSS_CLICK_ZONE,
    left: '0px',
    justifyContent: 'flex-start',
});

const nextClickZone = styled({
    ...CSS_CLICK_ZONE,
    right: '0px',
    justifyContent: 'flex-end'
});
