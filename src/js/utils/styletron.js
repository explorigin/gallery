import Styletron from 'styletron';
import { injectStyle as _injectStyle } from 'styletron-utils';

import { h } from './element';


const styletronSingleton = new Styletron();

export function injectStyle(styles) {
    return _injectStyle(styletronSingleton, styles);
}

export function styled(styles, tagName='div') {
    const className = injectStyle(styles);
    return (...props) => {
        const attrIndex = props.length && typeof props[0] === 'object' && !Array.isArray(props[0]) ? 0 : -1;
        const attrs = (
            attrIndex === -1
            ? { className }
            : {
                ...props[0],
                className: `${className} ${props[0].className || ''}`.trim()
            }
        );
        const children = props.slice(attrIndex + 1);
        return h(tagName, attrs, ...children);
    };
}
