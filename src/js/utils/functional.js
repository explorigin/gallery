// @flow
import { asArray, isFunction } from 'trimkit';


export function computed(dependencies, fn) {
    let val = undefined;
    let version = 0;
    if (isFunction(dependencies)) {
        fn = dependencies;
        dependencies = [];
    }
    let previousVersions = [];

    const accessor = function _computed() {
        const currentVersions = dependencies.map(d => d.v());

        if (currentVersions.some((v, i) => v !== previousVersions[i])
            || !dependencies.length
        ) {
            previousVersions = currentVersions;
            version++;
            val = fn.apply(null, dependencies.map(d => d()));
        }
        return val;
    };
    accessor.v = function() {
        accessor();
        return version;
    };

    return accessor;
}


export function observable(store, callback) {
    let version = 0;

    const accessor = function _prop() {
        const args = asArray(arguments);
        if (args.length) {
            version += 1;
            store = args[0];
            if (callback) {
                callback(store);
            }
        }
        return store;
    };
    accessor.v = () => version;

    return accessor;
}
