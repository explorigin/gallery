import { injectStyle } from './styletron';


export function h(tagName, ...rest) {
    if (!rest.length || typeof rest[0] !== 'object') {
        return [tagName, ...rest];
    }

    const attrs = rest[0];
    const children = rest.slice(1);

    if (attrs.css) {
        const styleClasses = injectStyle(attrs.css);
        attrs.className = ((attrs.className || '') + ` ${styleClasses}`).trim();
        delete attrs.css;
    }

    return [tagName, attrs, ...children];
}
