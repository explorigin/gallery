import { mount, addons } from 'starlight-js';

import { h } from './js/utils/element';
import { getRouter } from './js/services/route';
import * as styles from './style/main.css';
import { MainView } from './js/views/MainView';


const router = getRouter((api, route) => {
    app.update({
        album: route.vars.album || null,
        image: route.vars.image,
    });
});
const app = mount(
    MainView,
    document.body,
    {
        ...addons,
        ...router,
        h,
    }
);
router.listen('home');
