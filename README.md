# Gallery • made with [Starlight](https://gitlab.com/explorigin/starlight)

An offline-first photo gallery.

## Supported Browsers

Gallery uses:

- Service Workers
- Subtle Crypto

...and is therefore not supported by Safari, IE or Edge.

## Credit

Created by [Timothy Farrell](https://github.com/explorigin)
